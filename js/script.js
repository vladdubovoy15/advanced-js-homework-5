"use strict";
const btn = document.getElementById("button");
btn.addEventListener("click", async function () {
  const ip = "https://api.ipify.org/?format=json";
  const ipResponse = await fetch(ip);
  const data = await ipResponse.json();

  const geoResponse = await fetch(`http://ip-api.com/json/${data.ip}`);
  const geoData = await geoResponse.json();
  const { continent, country, region, city, district } = geoData;
  const ul = document.createElement("ul");
  btn.after(ul);

  ul.innerHTML = `<li>continent: ${continent}</li>
                    <li>country: ${country}</li>
                    <li>region: ${region}</li>
                    <li>city: ${city}</li>
                    <li>district: ${district}</li>`;
});
